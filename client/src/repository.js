import axios from 'axios';

const BASE_URL = '.';

export function getActivities() {
	return axios.get(`${BASE_URL}/api/activity/list`)
		.then(response => response.data);
}

export function deleteActivity(data){
	return axios.post(`${BASE_URL}/api/activity/delete`, {
		"_id": data.id,
		"_rev": data.rev
	  })
		.then(response => response.data)
		.catch(err => Promise.reject(err.message));
}
export function createActivity(data) {
	return axios.post(`${BASE_URL}/api/activity/create`, {
		"type": data.type, 
		"name": data.name, 
		"date": data.date, 
		"properties": data.properties, 
		"note": data.note
	  })
		.then(response => {
			{ console.log('repo', response.data);
			 return response.data}
		})
		.catch(err => Promise.reject(err.message));
}

export function updateActivity(data) {
	return axios.post(`${BASE_URL}/api/activity/update`, {
		"type": data.type, 
		"name": data.name, 
		"date": data.date, 
		"properties": data.properties, 
		"note": data.note,
		"_id": data._id,
		"_rev": data._rev
	  })
		.then(response => {
			return response.data
		})
		.catch(err => Promise.reject(err.message));
}
