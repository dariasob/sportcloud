import { mount } from '@vue/test-utils'
import EditActivityModal from '../src/components/EditActivityModal.vue'
import Vuetify from 'vuetify';
import Vue from 'vue';

Vue.use(Vuetify);


test('displays messages', async () => {
  // mount() returns a wrapped Vue component we can interact with
  const wrapper = mount(EditActivityModal, {
    created() {
        this.$vuetify.lang = {
          t: () => {},
      };
    },
    propsData: {
        activity: {type: "Bieganie", date: '2020-11-14'}
      }
  })

  // Assert the rendered text of the component
  expect(wrapper.text()).toContain('Edycja aktywności')

  
})