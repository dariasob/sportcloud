

import { mount } from '@vue/test-utils'
import Login from '../src/components/Login.vue'


test('correct login page without error message', async () => {
  // mount() returns a wrapped Vue component we can interact with
  const wrapper = mount(Login)

  // Assert the rendered text of the component
  expect(wrapper.text()).toContain('Logowanie')

  const button = wrapper.find('#saveBtn')

  expect(wrapper.text()).not.toMatch(/Wypełnij wymagane pola/)

  await button.trigger('click')
  await wrapper.setData({ errorsPresent: true })

  expect(wrapper.text()).toMatch(/Wypełnij wymagane pola/)
})