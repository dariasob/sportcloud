const JwtStrategy = require('passport-jwt').Strategy;
const ExtractJwt = require('passport-jwt').ExtractJwt;
//const key = require('./keys').secret;
const key = '3k2ds'

const opts = {};
opts.jwtFromRequest = ExtractJwt.fromAuthHeaderAsBearerToken();
opts.secretOrKey = key;

let mydb, cloudant;

if (process.env.CLOUDANT_URL){
  cloudant = Cloudant(process.env.CLOUDANT_URL);
}
if(cloudant) {
  //database name
  dbName = 'sportdb';

  // Create a new "sportdb" database.
  cloudant.db.create(dbName, function(err, data) {
    if(!err) //err if database doesn't already exists
      console.log("Created database: " + dbName);
  });

  // Specify the database we are going to use (sportdb)...
  mydb = cloudant.db.use(dbName);

}

module.exports = passport => {
    passport.use(
        new JwtStrategy(opts, (jwt_payload, done) => {
            //User.findById(jwt_payload._id).then(user => {
            mydb.find({'selector': {
                '_id': jwt_payload._id
              } },
            // .get(jwt_payload._id, 
                function(err, user) {
                if (user) return done(null, user);
                return done(null, false);
            }).catch(err => console.log("passport: ", err)); 
        })
    );
};
