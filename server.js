var express = require("express");
var app = express();
var cfenv = require("cfenv");
var bodyParser = require('body-parser')
const path = require('path');
const bcrypt = require('bcryptjs');
const jwt = require('jsonwebtoken');
const JwtStrategy = require('passport-jwt').Strategy;
const ExtractJwt = require('passport-jwt').ExtractJwt;
const passport = require('passport');
const key = '3k2ds';

const opts = {};
opts.jwtFromRequest = ExtractJwt.fromAuthHeaderAsBearerToken();
opts.secretOrKey = key;

// parse application/x-www-form-urlencoded
app.use(bodyParser.urlencoded({ extended: false }))

// parse application/json
app.use(bodyParser.json())

let mydb, cloudant;
var vendor; // Because the MongoDB and Cloudant use different API commands, we
            // have to check which command should be used based on the database
            // vendor.
var dbName = 'sportdb';

// Use the passport Middleware
app.use(passport.initialize());
passport.use(
  new JwtStrategy(opts, (jwt_payload, done) => {
      //User.findById(jwt_payload._id).then(user => {
      if(!mydb) {
        console.log("No database.");
        return;
      }
      mydb.find({'selector': {
          '_id': jwt_payload._id
        } 
      }, function(err, user) {
          if (user) return done(null, user);
          return done(null, false);
      })
  })
);
 // .get(jwt_payload._id,
//require('./passport')(passport);


var insertOne = {};
var getAll = {};

app.post("/api/activity/create", function (req, response) {
  //var userName = request.body.name;
  //var doc = { "name" : userName };
  var activity = {
    "type": req.body.type, 
    "name": req.body.name, 
    "date": req.body.date, 
    "properties": req.body.properties, 
    "note": req.body.note
  };
  if(!mydb) {
    console.log("No database.");
    response.send(activity);
    return;
  }
  mydb.insert(activity, function(err, body, header) {
    if (err) {
      console.log('[sportdb.insert] ', err.message);
      response.send("Error");
      return;
    }
    console.log('body', body);
    activity._id = body.id;
    response.send(activity);
  });
  //insertOne[vendor](activity, response);
});

app.post("/api/activity/update", function (req, response) {
  var names = [];
  var activity = {
    "type": req.body.type, 
    "name": req.body.name, 
    "date": req.body.date, 
    "properties": req.body.properties, 
    "note": req.body.note,
    "_id": req.body._id,
    "_rev": req.body._rev
  };
  if(!mydb) {
    response.json(names);
    return;
  }
  mydb.insert(activity, function(err, body, header) {
    if (err) {
      console.log('[sportdb.update] ', err.message);
      response.send("Error");
      return;
    }
    activity._id = body.id;
    response.send(activity);
  });
});

app.get("/api/activity/list", function (request, response) {
  var names = [];
  //var user = request.body.token;
  if(!mydb) {
    response.json(names);
    return;
  }
  var activitiesList = [];  
  mydb.list({ include_docs: true }, function(err, activities) {
    if (err) 
      return response.send({});
    
    activities.rows.forEach(function(row) {
      if(row.doc.type)
        activitiesList.push(row.doc);
    });
    return response.send( {activitiesList} );    
  });
});

app.post("/api/activity/delete", function (req, response) {

  if(!mydb) {
    console.log("No database.");
    response.send(activity);
    return;
  }
  mydb.destroy(req.body._id, req.body._rev, function(err, body, header) {
    if (err) {
      console.log('[sportdb.delete] ', err.message);
      response.send("Error");
      return;
    }
    response.send({ message: 'Aktywność usunięta!' });
  });
  //insertOne[vendor](activity, response);
});

app.post("/api/activity/register", (req, res) => {
  let {
      username,
      password,
      confirm_password
  } = req.body
  if (password !== confirm_password) {
      return res.status(400).json({
          msg: "Password do not match."
      });
  }
  
  //Check for the unique Username
  mydb.find({'selector': {
      'username': req.body.username
    }
  }, (err, user) => {
      if (user) {
          return res.status(400).json({
              msg: "Username is already taken."
          });
      }
  })
  // The data is valid and new we can register the user
  var newUser = {
      "username" : username,
      "password": password
  };
  // Hash the password
  bcrypt.genSalt(10, (err, salt) => {
      bcrypt.hash(newUser.password, salt, (err, hash) => {
          if (err) throw err;
          newUser.password = hash;
          mydb.insert(newUser, function(err, body, header) {
            if (err) {
              console.log('[sportdb.insert] ', err.message);
              res.send("Error");
              return;
            }
            newUser._id = body.id;
            res.send(newUser);
          });
          // newUser.save().then(user => {
          //     return res.status(201).json({
          //         success: true,
          //         msg: "Hurry! User is now registered."
          //     });
          // });
      });
  });
});

app.post("/api/activity/login", (req, res) => {
  console.log("login start");
  // User.findOne({
  //     username: req.body.username
  // }).then(user => {
    mydb.find({'selector': {
          'username': req.body.username
        }
      }, (err, user) => {
      console.log("user", user);
      if (!user) {
          return res.status(404).json({
              msg: "Username is not found.",
              success: false
          });
      }
      // If there is user we are now going to compare the password
      bcrypt.compare(req.body.password, user.docs[0].password).then(isMatch => {
          if (isMatch) {
              // User's password is correct and we need to send the JSON Token for that user
              const payload = {
                  _id: user.docs[0]._id,
                  username: user.docs[0].username
              }
              jwt.sign(payload, key, {
                  expiresIn: 604800
              }, (err, token) => {
                  res.status(200).json({
                      success: true,
                      token: `Bearer ${token}`,
                      user: user.docs[0],
                      msg: "Hurry! You are now logged in."
                  });
              })
          } else {
              return res.status(404).json({
                  msg: "Incorrect password.",
                  success: false
              });
          }
      })
  });
});


app.get("/api/activity/info", passport.authenticate('jwt', {
  session: false
}), (req, res) => {
  console.log('server info ', req.user)
  return res.json({
    user: req.user.docs[0]
  });
});


// load local VCAP configuration  and service credentials
var vcapLocal;
try {
  vcapLocal = require('./vcap-local.json');
  console.log("Loaded local VCAP", vcapLocal);
} catch (e) { }

const appEnvOpts = vcapLocal ? { vcap: vcapLocal} : {}

const appEnv = cfenv.getAppEnv(appEnvOpts);

if (appEnv.services['cloudantNoSQLDB'] || appEnv.getService(/[Cc][Ll][Oo][Uu][Dd][Aa][Nn][Tt]/)) {

  // Load the Cloudant library.
  var Cloudant = require('@cloudant/cloudant');

  // Initialize database with credentials
  if (appEnv.services['cloudantNoSQLDB']) {
    // CF service named 'cloudantNoSQLDB'
    cloudant = Cloudant(appEnv.services['cloudantNoSQLDB'][0].credentials);
  } else {
     // user-provided service with 'cloudant' in its name
     cloudant = Cloudant(appEnv.getService(/cloudant/).credentials);
  }
} else if (process.env.CLOUDANT_URL){
  cloudant = Cloudant(process.env.CLOUDANT_URL);
}
if(cloudant) {
  //database name
  dbName = 'sportdb';

  // Create a new "sportdb" database.
  cloudant.db.create(dbName, function(err, data) {
    if(!err) //err if database doesn't already exists
      console.log("Created database: " + dbName);
  });

  // Specify the database we are going to use (sportdb)...
  mydb = cloudant.db.use(dbName);

  vendor = 'cloudant';
}

//serve static file (index.html, images, css)
//app.use(express.static(__dirname + '/views'));

app.use(express.static(path.join(__dirname, '/client/dist')));

app.get('/', (req,res) => {
  res.sendFile(path.join(__dirname, '/client/dist/index.html'));
});



var port = process.env.PORT || 3000
app.listen(port, function() {
    console.log("To view your app, open this link in your browser: http://localhost:" + port);
});
