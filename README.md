# Aplikacja Sport Tracker
Aplikacja Sport Tracker to projekt stworzony w ramach przedmiotu Przetwarzanie w chmurach obliczeniowych. Jest to prosta aplikacja internetowa służaca do gromadzenia danych dotyczących aktywności sportowej użytkownika. Zalogowany użytkownik ma możliwość dodania aktywności o jednym z dostępnych typów lub wpisania swojej własnej nazwy. W zależności od wybranego typu dostępne są różne parametry opisujące aktywność. 

Aplikacja składa się z warstwy klienckiej napisanej z użyciem biblioteki [Vue](https://vuejs.org/), warstwy serwerowej stworzonej w technologii [Node.js](https://nodejs.org/en/) oraz bazy danych NoSQL. Aplikacja wykorzystuje bibliotekę [Express](https://expressjs.com) oraz [Cloudant noSQL DB service](https://console.bluemix.net/catalog/services/cloudant-nosql-db) do dodawania, edycji, usuwania i odczytywania poszczególnych danych. 

![Strona glowna](/client/src/assets/sportappimg.png "Strona główna")
