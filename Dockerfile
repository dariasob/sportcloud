FROM node:10 AS ui-build
WORKDIR /app
COPY client/ ./client/
RUN cd client && npm install && npm run build


FROM node:10
WORKDIR /app

COPY --from=ui-build /app/client/dist ./client/dist
# ADD views /app/views
COPY package.json .
COPY server.js .
COPY passport.js .

RUN cd /app; npm install

ENV NODE_ENV production
ENV PORT 8080
EXPOSE 8080

# WORKDIR /app
CMD [ "npm", "start" ]
